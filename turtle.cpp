#include <iostream>
#include "tbuffer.h"
#include "turtle.h"

Turtle::Turtle(){
    x = 0;
    y = 0;
    angle = 0;
    face = nearestDirection(angle);
}

Turtle::Turtle(int initx = 0, int inity = 0){
    x = initx;
    y = inity;
    angle = 0;
    face = nearestDirection(angle);
}

Turtle::Turtle(int initx = 0, int inity = 0, int degree = 0){
    x = initx;
    y = inity;
    angle = degree;
    face = nearestDirection(angle);
}

Turtle::Turtle(int initx, int inity, int degree, int width, int height){
    x = initx;
    y = inity;
    angle = degree;
    face = nearestDirection(angle);
    MAX_WIDTH = width;
    MAX_HEIGHT = height;
}

char* Turtle::head(){
    std::string headFace;
    switch (face){
        case UP:
            headFace="▲";
            break;
        case UR:
            headFace="◥";
            break;
        case RT:
            headFace="▶";
            break;
        case RD:
            headFace="◢";
            break;
        case DN:
            headFace="▼";
            break;
        case DL:
            headFace="◣";
            break;
        case LT:
            headFace="◀";
            break;
        case LU:
            headFace="◤";
            break;
        default:
            headFace=" ";
    }
    return (char*)headFace.c_str();
}

Directions Turtle::nearestDirection(int degree){
    degree = degree % 360;
    if(degree <= 360 && degree >= 338){
        return UP;
    }
    Directions nearest{UR};
    int diff = 360;
    
    for(int d=0; d<=360; d+=45){
        if(abs(static_cast<int>(degree)-d)<diff){
            diff = abs(static_cast<int>(degree) - d);
            nearest = static_cast<Directions>(d);
        }
    }
    return nearest;
}

short Turtle::edgeBitMap(std::string c){
    if(c == "╳"){ return 170; }
    else if(c == "╲"){ return 136; }
    else if(c == "┤"){ return 81; }
    else if(c == "┼"){ return 85; }
    else if(c == "┬"){ return 84; }
    else if(c == "┐"){ return 80; }
    else if(c == "┴"){ return 69; }
    else if(c == "─"){ return 68; }
    else if(c == "┘"){ return 65; }
    else if(c == "╴"){ return 64; }
    else if(c == "╱"){ return 34; }
    else if(c == "├"){ return 21; }
    else if(c == "┌"){ return 20; }
    else if(c == "│"){ return 17; }
    else if(c == "╷"){ return 16; }
    else if(c == "└"){ return 5; }
    else if(c == "╶"){ return 4; }
    else if(c == "╵"){ return 1; }
    else { return 0; }
}

std::string Turtle::bitMapChar(short m){
    switch(m){
        case 170: return "╳";
        case 136: return "╲";
        case 81: return "┤";
        case 85: return "┼";
        case 84: return "┬";
        case 80: return "┐";
        case 69: return "┴";
        case 68: return "─";
        case 65: return "┘";
        case 64: return "╴";
        case 34: return "╱";
        case 21: return "├";
        case 20: return "┌";
        case 17: return "│";
        case 16: return "╷";
        case 5: return "└";
        case 4: return  "╶";
        case 1: return "╵";
        default:  return " ";
    }
    return " ";
}

void Turtle::setSize(int width, int height){
    MAX_WIDTH = width;
    MAX_HEIGHT = height;
}

void Turtle::setyx(int posy, int posx, int d){
    y = posy;
    x = posx;
    angle = d;
    face = nearestDirection(angle);
}

std::string Turtle::fd(std::string under=""){
    std::string tailChar("");
    switch(face){
        case UP:
            if(y>0){
                y--;
                tailChar="│";
            }
            break;
        case UR:
            if(y>0 && x<(MAX_WIDTH-1)){
                y--;
                x++;
                tailChar="╱";
            }
            break;
        case RT:
            if(x<(MAX_WIDTH-1)){
                x++;
                tailChar="─";
            }
            break;
        case RD:
            if(y<(MAX_HEIGHT-1) && x<(MAX_WIDTH-1)){
                y++;
                x++;
                tailChar="╲";
            }
            break;
        case DN:
            if(y<(MAX_HEIGHT-1)){
                y++;
                tailChar="│";
            }
            break;
        case DL:
            if(y<(MAX_HEIGHT-1) && x>0){
                y++;
                x--;
                tailChar="╱";
            }
            break;
        case LT:
            if(x>0){
                x--;
                tailChar="─";
            }
            break;
        case LU:
            if(y>0 && x>0){
                y--;
                x--;
                tailChar="╲";
            }
            break;
    }
    // return bitMapChar(edgeBitMap(tailChar)|edgeBitMap(under));
    return tailChar;
}

std::string Turtle::bk(std::string under=""){
    std::string tailChar("");
    switch(face){
        case UP:
            if(y<(MAX_HEIGHT-1)){
                y++;
                tailChar="│";
            }
            break;
        case UR:
            if(y<(MAX_HEIGHT-1) && x>0){
                y++;
                x--;
                tailChar="╱";
            }
            break;
        case RT:
            if(x>0){
                x--;
                tailChar="─";
            }
            break;
        case RD:
            if(y>0 && x>0){
                y--;
                x--;
                tailChar="╲";
            }
            break;
        case DN:
            if(y>0){
                y--;
                tailChar="│";
            }
            break;
        case DL:
            if(y>0 && x<(MAX_WIDTH-1)){
                y--;
                x++;
                tailChar="╱";
            }
            break;
        case LT:
            if(x<(MAX_WIDTH-1)){
                x++;
                tailChar="─";
            }
            break;
        case LU:
            if(y<(MAX_HEIGHT-1) && x<(MAX_WIDTH-1)){
                y++;
                x--;
                tailChar="╲";
            }
            break;
    }
    // return bitMapChar(edgeBitMap(tailChar)|edgeBitMap(under));
    return tailChar;
}

void Turtle::rt(int degree){ 
    angle = (angle + degree) % 360;
    if(angle < 0){
        angle += 360;
    }
    face = nearestDirection(angle);
}

void Turtle::lt(int degree){ 
    angle = (angle - degree) % 360;
    if(angle < 0){
        angle += 360;
    }
    face = nearestDirection(angle);
}

