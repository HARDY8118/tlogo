#include <iostream>
#include <ncurses.h>
#include <string>
#include <regex>
#include "turtle.h"
#include "tbuffer.h"

WINDOW* stdscr;
WINDOW* inputwin;
WINDOW* errwin;
int ROWS,COLS;
TBuffer* screen;
Turtle* turtle;

void init(){
    setlocale(LC_ALL,"");
    stdscr = initscr();
    getmaxyx(stdscr,ROWS,COLS);

    nodelay(stdscr, false);
    refresh();

    errwin = newwin(1,COLS-16,ROWS-1,17);
    wrefresh(errwin);
}

void showerror(std::string err){
    werase(errwin);
    mvwprintw(errwin,0, COLS-18-err.length(),"%s",err.c_str());
    wrefresh(errwin);
}

void initinput(){
    inputwin = newwin(1,16,ROWS-1,0);
}

void getinput();

void parsecmd(std::string c){
    static std::regex cmdrargs("^([a-zA-Z]+)[[:s:]]([0-9]+)$");
    static std::regex cmdr("^([a-z]+)|([A-Z]+)$");

    static std::regex fdr("(fd|FD|forward|FORWARD)[[:s:]]([0-9]+)");
    static std::regex bkr("(bk|BK|back|BACK)[[:s:]]([0-9]+)");
    static std::regex rtr("(rt|RT|right|RIGHT)[[:s:]]([0-9]+)");
    static std::regex ltr("(lt|LT|left|LEFT)[[:s:]]([0-9]+)");

    static std::regex qtr("(q|e|Q|E|quit|exit|QUIT|EXIT)");
    static std::regex hmr("(home|HOME)");
    static std::regex clr("(clean|CLEAN)");
    static std::regex csr("(cs|CS)");
    static std::regex htr("(ht|HT)");
    static std::regex str("(st|ST)");
    static std::regex pur("(pu|PU)");
    static std::regex pdr("(pd|PD)");

    std::smatch m;
    if(regex_match(c,cmdrargs)){
        if(regex_search(c,m,fdr)){
            screen->fd(stoi(m[2]));
            screen->draw();
        }else if(regex_search(c,m,bkr)){
            screen->bk(stoi(m[2]));
            screen->draw();
        }else if(regex_search(c,m,rtr)){
            screen->rt(stoi(m[2]));
            screen->draw();
        }else if(regex_search(c,m,ltr)){
            screen->lt(stoi(m[2]));
            screen->draw();
        }else{
            showerror("Invalid command: "+c);
        }
    }else{
        if(regex_match(c,qtr)){
            return;
        }else if(regex_match(c,hmr)){
            turtle->setyx(ROWS/2,COLS/2,0);
            // refresh();
            screen->draw();
        }else if(regex_match(c,clr)){
            screen->clear();
            screen->draw();
        }else if(regex_match(c,csr)){
            screen->clear();
            turtle->setyx(ROWS/2,COLS/2,0);
            refresh();
        }else if(regex_match(c,htr)){
            turtle->showTurtle = false;
            screen->draw();
        }else if(regex_match(c,str)){
            turtle->showTurtle = true;
            screen->draw();
        }else if(regex_match(c,pur)){
            turtle->penDown = false;
            screen->draw();
        }else if(regex_match(c,pdr)){
            turtle->penDown = true;
            screen->draw();
        }else{
            showerror("Invalid command: "+c);
        }
    }
    getinput();
}

void getinput(){
    werase(inputwin);
    mvwprintw(inputwin,0,0,"%s","> ");
    wrefresh(inputwin);

    char cmd[16];
    wgetstr(inputwin,cmd);
    std::string cmd_s(cmd);
    parsecmd(cmd);
}

int main(){
    try{
        init();

        screen = new TBuffer(stdscr, ROWS-1, COLS);
        turtle = new Turtle(COLS/2, ROWS/2, 0);
        screen->attach(turtle);

        screen->draw();

        initinput();
        getinput();
    }catch(...){
        std::cout<<"Error"<<std::endl;
    }
    endwin();

    free(turtle);
    free(screen);

    exit(0);
    return 0;
}
