#include <cmath>
#include <string>

#ifndef TURTLE_H
#define TURTLE_H

enum Directions {
    UP = 0, 
    UR = 45, 
    RT = 90, 
    RD = 135, 
    DN = 180, 
    DL = 225, 
    LT = 270, 
    LU = 315
};

struct Turtle{
    unsigned int x{}, y{};
    Directions face{UP};
    int angle;
    bool penDown{true};
    bool showTurtle{true};

    int MAX_HEIGHT, MAX_WIDTH;

    Turtle();
    Turtle(int, int);
    Turtle(int, int, int);
    Turtle(int, int, int, int, int);

    char* head();

    Directions nearestDirection(int);

    static short edgeBitMap(std::string);
    static std::string bitMapChar(short);

    void setSize(int, int);
    void setyx(int, int, int);
    
    // https://en.wikipedia.org/wiki/MSWLogo
    std::string fd(std::string);
    std::string bk(std::string);
    void rt(int);
    void lt(int);
    void home();
    void cs();
    void ht();
    void st();
    void pu();
    void pd();
    void ct();

};

#endif

