#include <ncurses.h>
#include <string>
#include "turtle.h"
#include "tbuffer.h"

#include <iostream>

TBuffer::TBuffer(WINDOW* scr){
    outwin = scr;
    getmaxyx(scr, rows, cols);
    rows--;
    fillBuffer();
}

TBuffer::TBuffer(WINDOW* scr, int h, int w){
    outwin = scr;
    rows = h;
    cols = w;
    fillBuffer();
}

void TBuffer::fillBuffer(){
    data = new std::string*[rows];
    for(int i=0; i<rows; i++){
        data[i] = new std::string[cols];
    }
}

void TBuffer::attach(Turtle* t){
    turtle = t;
}

void TBuffer::draw(){
    werase(outwin);
    for(int r=0; r<rows; r++){
        for(int c=0; c<cols; c++){
            mvwprintw(outwin, r, c, "%s", data[r][c].c_str());
        }
    }
    if(turtle->showTurtle){
        mvwprintw(outwin, turtle->y, turtle->x, "%s", turtle->head());
    }
    refresh();
}

int TBuffer::getRows(){
    return rows;
}

int TBuffer::getCols(){
    return cols;
}

std::string TBuffer::dataAt(int row, int col){
    return data[row][col];
}

void TBuffer::clear(){
    for(int r=0; r<rows; r++){
        for(int c=0; c<cols; c++){
            data[r][c]=" ";
        }
    }
}

void TBuffer::fd(int steps){
    while(steps--){
        if(turtle->penDown){
            data[turtle->y][turtle->x] = turtle->fd(dataAt(turtle->y, turtle->x));
        }else{
            turtle->fd(dataAt(turtle->y, turtle->x));
        }
    }
    draw();
}

void TBuffer::bk(int steps){
    while(steps--){
        if(turtle->penDown){
            data[turtle->y][turtle->x] = turtle->bk(dataAt(turtle->y, turtle->x));
        }else{
            turtle->bk(dataAt(turtle->y, turtle->x));
        }
    }
    draw();
}

void TBuffer::rt(int degree){
    turtle->rt(degree);
    draw();
}

void TBuffer::lt(int degree){
    turtle->lt(degree);
    draw();
}

TBuffer::~TBuffer(){
    for(int i=0; i<rows; i++){
        free(data[i]);
    }
    free(data);
}
