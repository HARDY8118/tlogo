#include <ncurses.h>
#include <string>
#include "turtle.h"

#ifndef TBUFFER_H
#define TBUFFER_H

class TBuffer{
    private:
        Turtle *turtle{};
        int rows{}, cols{};
        std::string** data;
        WINDOW* outwin;

        void fillBuffer();

    public:
        TBuffer(WINDOW*);
        TBuffer(WINDOW*, int, int);

        void attach(Turtle*);

        const char* turtleHead();
        void draw();
        int getRows();
        int getCols();
        std::string dataAt(int, int);
        void clear();

        void fd(int);
        void bk(int);
        void rt(int);
        void lt(int);
    
        ~TBuffer();
};

#endif

